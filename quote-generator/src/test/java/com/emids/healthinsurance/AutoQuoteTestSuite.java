package com.emids.healthinsurance;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.emids.healthinsurance.service.QuoteGeneratorTest;

import junit.framework.TestCase;

/**
 * Unit test for all the testcase execution.
 */
@RunWith(Suite.class)
@SuiteClasses({ QuoteGeneratorTest.class })
public class AutoQuoteTestSuite extends TestCase {

}
