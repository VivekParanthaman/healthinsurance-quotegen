package com.emids.healthinsurance.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.emids.healthinsurance.QuoteConstant;
import com.emids.healthinsurance.pojo.Customer;
import com.emids.healthinsurance.pojo.Habits;
import com.emids.healthinsurance.pojo.HealthStatus;
import com.emids.healthinsurance.service.impl.QuoteGeneratorSchemeImpl;

/**
 * 
 * @author Vivek
 *
 */
public class QuoteGeneratorTest {

	private double basePremium;
	private Customer customer;
	private Habits habits;
	private HealthStatus healthStatus;
	private IQuoteGeneratorScheme quoteGenerator;
	static final double ageBandVal = 6655.000000000002;
	static final double genderBand = 5100.0;
	static final double healthStat = 5050.0;
	static final double habitsStat = 5000.0;
	static final int expectedResult = 6856;

	/**
	 * Initialization of all the objects and data necessary for the test suits
	 * to run successfully
	 */
	@Before
	public void initValues() {
		basePremium = QuoteConstant.MINOR_BASE_QUOTE;
		customer = new Customer();
		customer.setName("Norman");
		customer.setAge(34);
		customer.setGender("male");
		habits = new Habits();
		habits.setAlcohol(true);
		habits.setDrugs(false);
		habits.setSmoking(false);
		habits.setExcercise(true);
		customer.setHabits(habits);
		healthStatus = new HealthStatus(false, false, true, false);
		customer.setHealthStatus(healthStatus);
		quoteGenerator = new QuoteGeneratorSchemeImpl();
	}

	/**
	 * A random atomic testcase to validate the rate calculated based upon the
	 * age-band a customer belongs to
	 */
	@Test
	public void testQuoteByAgeBand() {
		basePremium = quoteGenerator.getQuoteByAgeBand(customer, basePremium);
		Assert.assertNotEquals(0, basePremium);
		Assert.assertEquals(ageBandVal, basePremium, 0.0);
	}

	/**
	 * A random testcase to validate the rate calculated at 2% for male
	 * customers
	 */
	@Test
	public void testQuoteByGender() {
		basePremium = quoteGenerator.getQuoteByGender(customer, basePremium);
		Assert.assertNotEquals(0, basePremium);
		Assert.assertEquals(genderBand, basePremium, 0.0);
	}

	/**
	 * Atomic testcase to calculate the quote-rate based upon existing
	 * healthstatus
	 */
	@Test
	public void testQuoteByHealthStatus() {
		basePremium = quoteGenerator.getQuoteByHealthStatus(customer, basePremium);
		Assert.assertNotEquals(0, basePremium);
		Assert.assertEquals(healthStat, basePremium, 0.0);
	}

	/**
	 * Atomic testcase to calculate the quote-rate based upon existing habits
	 * (good vs bad)
	 */
	@Test
	public void testQuoteByHabits() {
		basePremium = quoteGenerator.getQuoteByHabits(customer, basePremium);
		Assert.assertNotEquals(0, basePremium);
		Assert.assertEquals(habitsStat, basePremium, 0.0);
	}

	/**
	 * An integrated test case to validate the given verify the expected result
	 * match-up with a delta of 0.2
	 */
	@Test
	public void testQuoteInTotalWithDelta() {
		basePremium = quoteGenerator.getQuoteByAgeBand(customer, basePremium);
		basePremium = quoteGenerator.getQuoteByGender(customer, basePremium);
		basePremium = quoteGenerator.getQuoteByHealthStatus(customer, basePremium);
		basePremium = quoteGenerator.getQuoteByHabits(customer, basePremium);
		Assert.assertNotEquals(0, basePremium);
		Assert.assertEquals(expectedResult, basePremium, 0.2);
	}

	/**
	 * An integrated test case to validate the given verify the expected result
	 * match-up with the derived one
	 */
	@Test
	public void testQuoteInTotalWithoutDelta() {
		basePremium = quoteGenerator.getQuoteByAgeBand(customer, basePremium);
		basePremium = quoteGenerator.getQuoteByGender(customer, basePremium);
		basePremium = quoteGenerator.getQuoteByHealthStatus(customer, basePremium);
		basePremium = quoteGenerator.getQuoteByHabits(customer, basePremium);
		Assert.assertNotEquals(0, basePremium);
		Assert.assertEquals(expectedResult, Math.round(basePremium), 0.0);
	}

}
