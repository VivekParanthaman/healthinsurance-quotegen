package com.emids.healthinsurance.service;

import com.emids.healthinsurance.pojo.Customer;

/**
 * 
 * @author Emids
 *
 */
public interface IQuoteGeneratorScheme {

	/**
	 * A point of contact to check the quote by gender, applies 2% if male
	 * 
	 * @param age
	 * @param gender
	 * @param calculatedPremium
	 * @return
	 */
	public double getQuoteByGender(Customer customer, double calculatedPremium);

	/**
	 * For a given age, if belongs to one of it, evaluates the quote
	 * 
	 * @param age
	 * @param calculatedPremium
	 * @return
	 */
	public double getQuoteByAgeBand(Customer customer, double calculatedPremium);

	/**
	 * Re-iterates the quote value by the health-status of the customer
	 * 
	 * @param age
	 * @param illnessCount
	 * @param calculatedPremium
	 * @return
	 */
	public double getQuoteByHealthStatus(Customer customer, double calculatedPremium);

	/**
	 * Re-iterates the quote value by the existing habits
	 * 
	 * @param age
	 * @param habitsount
	 * @param calculatedPremium
	 * @return
	 */
	public double getQuoteByHabits(Customer customer, double calculatedPremium);

}
