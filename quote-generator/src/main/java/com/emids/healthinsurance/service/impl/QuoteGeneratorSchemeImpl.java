package com.emids.healthinsurance.service.impl;

import com.emids.healthinsurance.QuoteConstant;
import com.emids.healthinsurance.pojo.Customer;
import com.emids.healthinsurance.service.IQuoteGeneratorScheme;
import com.emids.healthinsurance.utils.QuotesGenUtil;

/**
 * 
 * @author Emids
 * 
 *         Implementations for different quote generator strategies
 *
 */
public class QuoteGeneratorSchemeImpl implements IQuoteGeneratorScheme {

	/**
	 * Implementation to generate the quote by gender
	 */
	@Override
	public double getQuoteByGender(Customer customer, double calculatedPremium) {
		if (QuotesGenUtil.isMale(customer.getGender()) && !QuotesGenUtil.isMinor(customer.getAge())) {
			calculatedPremium *= 1.02;
		}
		return calculatedPremium;
	}// ..end of the method

	/**
	 * Implementation to generate the quote by age-band
	 */
	@Override
	public double getQuoteByAgeBand(Customer customer, double calculatedPremium) {
		double age = customer.getAge();
		if (QuotesGenUtil.filterAgeBand(age).contains(QuoteConstant.BAND_0) && QuotesGenUtil.filterAgeBand(age).size() == 1) {
			return calculatedPremium;
		}
		if (QuotesGenUtil.filterAgeBand(age).contains(QuoteConstant.BAND_1)) {
			calculatedPremium *= 1.1;
		}
		if (QuotesGenUtil.filterAgeBand(age).contains(QuoteConstant.BAND_2)) {
			calculatedPremium *= 1.1;
		}
		if (QuotesGenUtil.filterAgeBand(age).contains(QuoteConstant.BAND_3)) {
			calculatedPremium *= 1.1;
		}
		if (QuotesGenUtil.filterAgeBand(age).contains(QuoteConstant.BAND_4)) {
			calculatedPremium *= 1.1;
		}
		if (QuotesGenUtil.filterAgeBand(age).contains(QuoteConstant.BAND_5)) {
			QuotesGenUtil.calculateQuoteBand5(age, calculatedPremium);
		}
		return calculatedPremium;
	}// ..end of the method

	/**
	 * Implementation to generate the quote by HealthStatus
	 */
	@Override
	public double getQuoteByHealthStatus(Customer customer, double calculatedPremium) {
		if (!QuotesGenUtil.isMinor(customer.getAge())) {
			int illnessCount = QuotesGenUtil.getIllnessList(customer.getHealthStatus());
			for (int i = 0; i < illnessCount; i++) {
				calculatedPremium *= 1.01;
			}
		}
		return calculatedPremium;
	}// ..end of the method

	/**
	 * Implementation to generate the quote by Habits
	 */
	@Override
	public double getQuoteByHabits(Customer customer, double calculatedPremium) {
		if (!QuotesGenUtil.isMinor(customer.getAge())) {
			int habitsount = QuotesGenUtil.getHabitsList(customer.getHabits());
			for (int i = 0; i < habitsount; i++) {
				calculatedPremium *= 1.03;
			}
		}
		return calculatedPremium;
	}// ..end of the method
}
