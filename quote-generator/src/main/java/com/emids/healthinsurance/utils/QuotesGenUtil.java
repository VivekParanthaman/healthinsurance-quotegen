package com.emids.healthinsurance.utils;

import java.util.ArrayList;
import java.util.List;

import com.emids.healthinsurance.QuoteConstant;
import com.emids.healthinsurance.pojo.Habits;
import com.emids.healthinsurance.pojo.HealthStatus;

/**
 * 
 * @author Emids
 *
 */
public class QuotesGenUtil {

	/**
	 * Utility to verify the age band
	 * 
	 * @param age
	 * @return true/false based upon evaluation
	 */
	public static boolean isMinor(double age) {
		return (age <= 18) ? QuoteConstant.YES : QuoteConstant.NO;
	}// ..end of the method

	/**
	 * Checking gender
	 * 
	 * @param gender
	 * @return
	 */
	public static boolean isMale(String gender) {
		return gender.trim().equalsIgnoreCase(QuoteConstant.GENDER_MALE) ? true : false;
	}// ..end of the method

	/**
	 * Checking gender
	 * 
	 * @param gender
	 * @return
	 */
	public static boolean isFemale(String gender) {
		return gender.trim().equalsIgnoreCase(QuoteConstant.GENDER_FEMALE) ? true : false;
	}// ..end of the method

	/**
	 * Get list of age-bands cust belongs by given age
	 * 
	 * @param age
	 * @return
	 */
	public static List<String> filterAgeBand(double age) {
		List<String> arr = new ArrayList<>();
		if (age <= 18) {
			arr.add(QuoteConstant.BAND_0);
		}
		if (age > 18.0) {
			arr.add(QuoteConstant.BAND_1);
		}
		if (age > 25.0) {
			arr.add(QuoteConstant.BAND_2);
		}
		if (age > 30.0) {
			arr.add(QuoteConstant.BAND_3);
		}
		if (age > 35.0) {
			arr.add(QuoteConstant.BAND_4);
		}
		if (age > 40.0) {
			arr.add(QuoteConstant.BAND_5);
		}
		return arr;
	}// ..end of the method

	/**
	 * Special case QuoteCalulation for cust above 40
	 * 
	 * @param age
	 * @param calculatedPremium
	 * @return
	 */
	public static double calculateQuoteBand5(double age, double calculatedPremium) {
		double diffb5 = age - 40.0;
		int slotsPassed = (int) (diffb5 / 5);
		for (int i = 0; i <= slotsPassed; i++) {
			calculatedPremium *= 1.20;
		}
		return calculatedPremium;
	}// ..end of the method

	/**
	 * Utility to get the illness count
	 * 
	 * @param healthStatus
	 * @return
	 */
	public static int getIllnessList(HealthStatus healthStatus) {
		int illnessCount = 0;
		if (healthStatus.hasBloodPressure()) {
			illnessCount += 1;
		}
		if (healthStatus.hasBloodSugar()) {
			illnessCount += 1;
		}
		if (healthStatus.hasHyperTension()) {
			illnessCount += 1;
		}
		if (healthStatus.isOverWeight()) {
			illnessCount += 1;
		}
		return illnessCount;
	}// ..end of the method

	/**
	 * Utility to get the total count of habits
	 * 
	 * @param healthStatus
	 * @return
	 */
	public static int getHabitsList(Habits habits) {
		int habitsCount = 0;
		if (habits.isAlcohol()) {
			habitsCount += 1;
		}
		if (habits.isDrugs()) {
			habitsCount += 1;
		}
		if (habits.isSmoking()) {
			habitsCount += 1;
		}
		if (habits.isExcercise()) {
			habitsCount -= 1;
		}
		return habitsCount;
	}// ..end of the method

}
