package com.emids.healthinsurance;

/**
 * 
 * @author Emids
 *
 */
public class QuoteConstant {

	public static final boolean YES = true;
	public static final boolean NO = false;
	public static final double MINOR_BASE_QUOTE = 5000;
	public static final String GENDER_MALE = "male";
	public static final String GENDER_FEMALE = "female";
	public static final String GENDER_OTHERS = "others";
	public static final String BAND_0 = "-18";
	public static final String BAND_1 = "18-25";
	public static final String BAND_2 = "25-30";
	public static final String BAND_3 = "30-35";
	public static final String BAND_4 = "35-40";
	public static final String BAND_5 = "40+";
	public static final String OVERWEIGHT = "overweight";
	public static final String BLOODPRESSURE = "bloodpressure";
	public static final String BLOODSUGAR = "bloodsugar";

}
