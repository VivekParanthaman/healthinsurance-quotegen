package com.emids.healthinsurance.pojo;

/**
 * 
 * @author Emids
 *
 */
public class Habits {

	private boolean smoking;
	private boolean alcohol;
	private boolean drugs;
	private boolean excercise;

	public boolean isSmoking() {
		return smoking;
	}

	public void setSmoking(boolean smoking) {
		this.smoking = smoking;
	}

	public boolean isAlcohol() {
		return alcohol;
	}

	public void setAlcohol(boolean alcohol) {
		this.alcohol = alcohol;
	}

	public boolean isDrugs() {
		return drugs;
	}

	public void setDrugs(boolean drugs) {
		this.drugs = drugs;
	}

	public boolean isExcercise() {
		return excercise;
	}

	public void setExcercise(boolean excercise) {
		this.excercise = excercise;
	}

	@Override
	public String toString() {
		return "Habits [smoking=" + smoking + ", alcohol=" + alcohol + ", drugs=" + drugs + ", excercise=" + excercise
				+ "]";
	}

}
