package com.emids.healthinsurance.pojo;

/**
 * 
 * @author Emids
 *
 */
public class HealthStatus {

	private boolean hyperTension;
	private boolean bloodPressure;
	private boolean overWeight;
	private boolean bloodSugar;

	public HealthStatus(boolean hyperTension, boolean bloodPressure, boolean overWeight, boolean bloodSugar) {
		this.hyperTension = hyperTension;
		this.bloodPressure = bloodPressure;
		this.overWeight = overWeight;
		this.bloodSugar = bloodSugar;
	}

	public boolean hasHyperTension() {
		return hyperTension;
	}

	public void setHyperTension(boolean hyperTension) {
		this.hyperTension = hyperTension;
	}

	public boolean hasBloodPressure() {
		return bloodPressure;
	}

	public void setBloodPressure(boolean bloodPressure) {
		this.bloodPressure = bloodPressure;
	}

	public boolean isOverWeight() {
		return overWeight;
	}

	public void setOverWeight(boolean overWeight) {
		this.overWeight = overWeight;
	}

	public boolean hasBloodSugar() {
		return bloodSugar;
	}

	public void setBloodSugar(boolean bloodSugar) {
		this.bloodSugar = bloodSugar;
	}

	@Override
	public String toString() {
		return "HealthStatus [hyperTension=" + hyperTension + ", bloodPressure=" + bloodPressure + ", overWeight="
				+ overWeight + ", bloodSugar=" + bloodSugar + "]";
	}

}
